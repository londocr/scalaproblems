//Nombre del proyecto
name := "ScalaProblems"

version := "0.1"

organization := "com.ciriscr"

scalaVersion := "2.10.0"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

//Casbah, Scalatest, akka, salat
libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"
)

// para compilar en varias versiones de scala
crossScalaVersions := Seq("2.9.2", "2.10.0")

// reduce the maximum number of errors shown by the Scala compiler
maxErrors := 30

// increase the time between polling for file changes when using continuous execution
pollInterval := 1000

// append several options to the list of options passed to the Java compiler
javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

// append -deprecation to the options passed to the Scala compiler
scalacOptions ++= Seq("-deprecation", "-optimise", "-explaintypes")


// set the main class for packaging the main jar
// 'run' will still auto-detect and prompt
// change Compile to Test to set it for the test jar
//mainClass in (Compile, packageBin) := Some("myproject.MyMain")

// set the main class for the main 'run' task
// change Compile to Test to set it for 'test:run'
//mainClass in (Compile, run) := Some("myproject.MyMain")


// only use a single thread for building
parallelExecution := true
