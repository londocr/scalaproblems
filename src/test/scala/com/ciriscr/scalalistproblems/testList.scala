package com.ciriscr.scalalistproblems

import _root_..
import _root_..
import _root_..
import _root_..
import _root_..
import _root_..
import _root_..
import _root_..
import org.scalatest.FunSuite

/**
 * Created by IntelliJ IDEA.
 * User: londo
 * Date: 01/01/12
 * Time: 08:24 PM
 */

class testList extends FunSuite {

  val l1 = List(1, 1, 2, 3, 5, 8)
  val l2 = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)

  test("P01") {
    assert(8 == Lists.last(l1))
  }

  test("P02") {
    assert(5 == Lists.penultimate(l1))
  }

  test("P03") {
    assert(2 == Lists.n_avo(2, l1))
  }

  test("P04") {
    assert(6 == Lists.length(l1))
  }

  test("P05") {
    assert(List(8, 5, 3, 2, 1, 1) === Lists.reverse(l1))
  }

  test("P06") {
    val l = List(1, 2, 3, 2, 1)
    assert(Lists.isPalindrome(l))
  }

  test("P07") {
    val l = List(List(1, 1), 2, List(3, List(5, 8)))
    assert(l1 === Lists.flatten(l))
  }

  test("P08") {
    assert(List('a, 'b, 'c, 'a, 'd, 'e) === Lists.compress(l2))
  }

}