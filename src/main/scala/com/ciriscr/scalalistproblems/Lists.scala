package com.ciriscr.scalalistproblems

/**
 * Created by IntelliJ IDEA.
 * User: londo
 * Date: 01/01/12
 * Time: 08:20 PM
 */

object Lists {

  def last[A](l: List[A]) = l.last

  def penultimate[A](l: List[A]) = l.takeRight(2).head

  def n_avo[A](n: Int, l: List[A]) = l(n)

  def length[A](l: List[A]) = l.length

  def reverse[A](l: List[A]) = l.reverse

  def isPalindrome[A](l: List[A]) = l.reverse == l

  def flatten(l: List[Any]): List[Any] = {
    l.flatMap{
      case x: List[_] => flatten(x)
      case e => List(e)
    }
  }

  def compress[A](l: List[A]) = {
    println(l.reduce((a,b) => a))
    l
  }

}